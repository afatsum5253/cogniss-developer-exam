const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputFile = "input2.json";
const inputURL = "https://jsonplaceholder.typicode.com/posts/1";
const outputFile = "output2.json";

output = {}
jsonfile.readFile(inputFile, function(err, body){
    console.log("loaded input file content", body);
    output= body;
    output.fake_emails = [];
    for (var i=0; i<output.names.length; i++) {
        var names = output.names[i];
        names = names.split("").reverse().join("");
        names = names + randomstring.generate(5) + "@gmail.com";
        output.fake_emails.push(names);

    }
    console.log("generated fake emails in output2");
    console.log("saving output file formatted with 2 space indenting");
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
      console.log("All done!");
    });
  });